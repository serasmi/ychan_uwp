﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ychan_uwp.ViewModels.Helpers
{
    class NavigationHelper : INavigationHelper
    {
        private Frame frame;
        private SplitView splitView;

        public NavigationHelper(
            SplitView splitView,
            Frame frame)
        {
            if(frame == null)
            {
                throw new ArgumentNullException();
            }

            if(splitView == null)
            {
                throw new ArgumentNullException();
            }

            this.frame = frame;
            this.splitView = splitView;
        }

        public void GoBack()
        {
            if (this.frame.CanGoBack)
            {
                this.frame.GoBack();
            }
        }

        public void NavigateTo(Type pageType, object parameter)
        {
            frame.Navigate(pageType, parameter);
        }
    }
}
