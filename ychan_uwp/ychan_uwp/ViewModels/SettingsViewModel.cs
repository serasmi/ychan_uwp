﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ychan_uwp.ViewModels.Helpers;

namespace ychan_uwp.ViewModels
{
    public class SettingsViewModel
    {
        private ICommand downloadsPicker;
        public ICommand DownloadsPicker
        {
            get
            {
                return downloadsPicker ??
                    (downloadsPicker = new Command((object arg) =>
                    {
                        FolderPickerAsync(false);
                    }));
            }
        }

        private ICommand downloadedPicker;
        public ICommand DownloadedPicker
        {
            get
            {
                return downloadedPicker ??
                    (downloadedPicker = new Command((object arg) =>
                    {
                        FolderPickerAsync(true);
                    }));
            }
        }

        private ICommand save;
        public ICommand Save
        {
            get
            {
                return save ??
                    (save = new Command((object arg) =>
                    {
                        settings.SaveSettings();
                    }));
            }
        }

        public Models.Settings settings;

        public SettingsViewModel()
        {
            settings = new Models.Settings();
        }

        private async Task FolderPickerAsync(bool downloaded)
        {
            var folderPicker = new Windows.Storage.Pickers.FolderPicker();
            folderPicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.Downloads;
            folderPicker.FileTypeFilter.Add("*");

            Windows.Storage.StorageFolder folder = await folderPicker.PickSingleFolderAsync();
            if(folder != null)
            {
                Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.AddOrReplace("PickedFolderToken", folder);

                if (downloaded)
                {
                    settings.Downloaded = folder.Path.ToString();
                }
                else
                {
                    settings.Downloads = folder.Path.ToString();
                }
            }
        }
    }
}
