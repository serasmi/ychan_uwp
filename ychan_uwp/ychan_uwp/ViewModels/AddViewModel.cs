﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Media.Imaging;
using ychan_uwp.Models;
using ychan_uwp.ViewModels.Helpers;

namespace ychan_uwp.ViewModels
{
    public class AddViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public event NotifyCollectionChangedEventHandler CollectionChanged = delegate { };
        
        private Services.IFilesAddress files;

        private string uriAddress { get; set; }
        public string UriAddress
        {
            get { return this.uriAddress; }
            set
            {
                if (string.IsNullOrEmpty(this.uriAddress) || !this.uriAddress.Equals(value) || numberOfFilesChanged)
                {
                    this.uriAddress = value;
                    this.CanExecute = true;
                    this.OnPropertyChanged();
                }
            }
        }

        private bool canExecute { get; set; }
        public bool CanExecute
        {
            get { return this.canExecute; }
            set
            {
                this.canExecute = value;
                this.OnPropertyChanged();
            }
        }


        private ObservableCollection<Models.ImageModel> images { get; set; }
        public ObservableCollection<Models.ImageModel> Images
        {
            get { return this.images; }
            set
            {
                this.images = value;
            }
        }

        private bool numberOfFilesChanged { get; set; }

        private uint numberOfFiles { get; set; }
        public uint NumberOfFiles
        {
            get { return numberOfFiles; }
            set
            {
                if(this.numberOfFiles != value)
                {
                    this.numberOfFilesChanged = true;
                    this.OnPropertyChanged();
                }
                else
                    this.numberOfFilesChanged = false;
            }
        }
        
        private ICommand downloadButton { get; set; }
        public ICommand DownloadButton
        {
            get
            {
                return downloadButton ??
                  (downloadButton = new Command((object arg) =>
                  {
                      GetThumbnails();
                  },
                  (object arg) => 
                  {
                      if (arg != null)
                          return (bool)arg;
                      return false;

                  } ));
            }
        }

        public AddViewModel()
        {
            this.Images = new ObservableCollection<Models.ImageModel>();
            this.CanExecute = false;
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OnCollecionChanged(NotifyCollectionChangedAction e, [CallerMemberName] string propertyName = null)
        {
            switch (e)
            {
                case NotifyCollectionChangedAction.Reset:
                    this.CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
                case NotifyCollectionChangedAction.Add:
                    this.CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, propertyName));
                    break;
            }
            
        }

        private async Task GetThumbnails()
        {
            downloadButton.CanExecute(false);
            this.files = new Services.ChanFiles(this.uriAddress);
            
            var imagesLinks = await this.files.GetThumbnailsAddresses();
            this.NumberOfFiles = (uint)imagesLinks.Count;

            this.Images.Clear();
            this.OnCollecionChanged(NotifyCollectionChangedAction.Reset);
            foreach (String imageLink in imagesLinks)
            {
                var test = await this.files.GetThumbnail(imageLink);
                this.Images.Add(test);
                this.OnCollecionChanged(NotifyCollectionChangedAction.Add);
            }

            await Task.Delay(15000);
            downloadButton.CanExecute(true);
        }
    }
}
