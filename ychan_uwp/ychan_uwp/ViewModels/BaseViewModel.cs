﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ychan_uwp.ViewModels.Helpers;
using ychan_uwp.Views;

namespace ychan_uwp.ViewModels
{
    public class BaseViewModel
    {
        INavigationHelper navigationHelper;
        ICommand navigateToSettings;
        ICommand navigateToDownloads;
        ICommand navigateToDownloaded;
        ICommand navigateToAdd;
        ICommand navigateToRemove;

        public ICommand NavigateToSettings
        {
            get
            {
                return navigateToSettings ??
                    (navigateToSettings = new Command((object arg) =>
                    {
                        navigationHelper.NavigateTo(
                            typeof(SettingsView), this);
                    }));
            }
        }

        public ICommand NavigateToDownloads
        {
            get
            {
                return navigateToDownloads ??
                    (navigateToDownloads = new Command((object arg) =>
                    {
                        navigationHelper.NavigateTo(
                            typeof(DownloadsView), this);
                    }));
            }
        }

        public ICommand NavigateToDownloaded
        {
            get
            {
                return navigateToDownloaded ??
                    (navigateToDownloaded = new Command((object arg) =>
                    {
                        navigationHelper.NavigateTo(
                            typeof(DownloadedView), this);
                    }));
            }
        }

        public ICommand NavigateToAdd
        {
            get
            {
                return navigateToAdd ??
                    (navigateToAdd = new Command((object arg) =>
                    {
                        navigationHelper.NavigateTo(
                            typeof(AddView), this);
                    }));
            }
        }

        public ICommand NavigateToRemove
        {
            get
            {
                return navigateToRemove ??
                    (navigateToRemove = new Command((object arg) =>
                    {
                        navigationHelper.NavigateTo(
                            typeof(RemoveView), this);
                    }));
            }
        }

        public BaseViewModel(INavigationHelper navigationHelper)
        {
            if(navigationHelper == null)
            {
                throw new ArgumentNullException();
            }

            this.navigationHelper = navigationHelper;
        }
    }
}
