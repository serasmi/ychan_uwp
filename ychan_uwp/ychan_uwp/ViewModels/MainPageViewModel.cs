﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ychan_uwp.Models;
using ychan_uwp.ViewModels.Helpers;

namespace ychan_uwp.ViewModels
{
    public sealed class MainPageViewModel : BaseViewModel
    {
        private ICommand hamburger;

        public ICommand Hamburger
        {
            get
            {
                return hamburger ??
                    (hamburger = new Command((object arg) =>
                    {
                        ChangePaneState();
                    }));
            }
        }

        public MenuPanel menu;

        public MainPageViewModel(INavigationHelper navigationHelper) : base(navigationHelper)
        {
            menu = new MenuPanel(false);
        }

        protected void ChangePaneState()
        {
            menu.IsPaneOpen = !menu.IsPaneOpen;
        }
    }
}
