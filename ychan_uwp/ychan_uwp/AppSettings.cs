﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ychan_uwp
{
    public static class AppSettings
    {
        private static ApplicationDataContainer appDataContainer;

        static AppSettings()
        {
            appDataContainer = ApplicationData.Current.LocalSettings;
        }

        public static void AddOrUpdate(
            string key,
            object value)
        {
            if (appDataContainer.Values.ContainsKey(key))
            {
                appDataContainer.Values[key] = value;
            }
            else
            {
                appDataContainer.Values.Add(key, value);
            }
        }

        public static bool Get(
            string key,
            out object value)
        {
            bool result = false;

            value = null;

            if (appDataContainer.Values.ContainsKey(key))
            {
                if (appDataContainer.Values.TryGetValue(key, out value))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}
