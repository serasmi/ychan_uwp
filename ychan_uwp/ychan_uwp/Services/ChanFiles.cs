﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Newtonsoft.Json;
using System.Linq;
using Windows.UI.Xaml.Controls;
using ychan_uwp.Models;

namespace ychan_uwp.Services
{
    class ChanFiles : IFilesAddress
    {
        private const string FILE_REGEX = @"<a class=""fileThumb"" href=[\""'](.+?)[\""'].+?>";
        private const string THUMB_REGEX = @"<img.+?src=[\""'](.+?)[\""'].+?>";

        Models.Settings settings;
        private string url;
        private string board;
        private string threadNumber;

        private Models.ChanPosts json;
        List<Models.ChanModel> chansWithFile;

        public ChanFiles(string UrlAddress)
        {
            this.settings = new Models.Settings();
            this.url = UrlAddress;
            //http://boards.4channel.org/p/thread/3486946

            string[] urlArray = this.url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            this.board = urlArray[2];
            this.threadNumber = urlArray[4];

            json = new Models.ChanPosts();
            chansWithFile = new List<Models.ChanModel>();
        }

        public void PrepareDatas()
        {
            ConsumeRestApi();

            chansWithFile = json.posts.Select(x => x).Where(s => s.filename != null).ToList();
        }

        private void ConsumeRestApi()
        {
            string requestUrl = $"http://a.4cdn.org/{this.board}/thread/{this.threadNumber}.json";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);

            var content = string.Empty;

            using (var response = (HttpWebResponse)request.GetResponseAsync().Result)
            {
                using (var stream = response.GetResponseStream())
                {
                    using(var sr = new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                    }
                }
            }

            json = JsonConvert.DeserializeObject<Models.ChanPosts>(content);
        }

        public void DownloadFiles(List<Uri> addresses)
        {
            throw new NotImplementedException();
        }

        public async Task<List<string>> GetFilesAddresses()
        {
            throw new NotImplementedException();
        }

        public async Task<Models.ImageModel> GetThumbnail(string thumbnailLink)
        {
            Models.ImageModel image = new Models.ImageModel();
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(thumbnailLink);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();

            Stream stream = response.GetResponseStream();
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);

            IRandomAccessStream randomAccessStream = await ConvertToRandomAccessStream(ms);

            BitmapImage bitmap = new BitmapImage();
            await bitmap.SetSourceAsync(randomAccessStream);
            image.Img = new Image() { Source = bitmap };

            return image;
        }

        public async Task<List<String>> GetThumbnailsAddresses()
        {
            this.PrepareDatas();

            List<String> addresses = new List<String>();

            addresses = this.chansWithFile.Select(x => $"http://i.4cdn.org/{this.board}/{x.tim}s.jpg").ToList();

            return addresses;
        }

        public static async Task<IRandomAccessStream> ConvertToRandomAccessStream(MemoryStream memoryStream)
        {
            var randomAccessStream = new InMemoryRandomAccessStream();
            var outputStream = randomAccessStream.GetOutputStreamAt(0);
            var dw = new DataWriter(outputStream);
            var task = Task.Factory.StartNew(() => dw.WriteBytes(memoryStream.ToArray()));
            await task;
            await dw.StoreAsync();
            await outputStream.FlushAsync();
            return randomAccessStream;
        }
    }
}
