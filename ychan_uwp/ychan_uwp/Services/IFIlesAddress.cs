﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ychan_uwp.Models;

namespace ychan_uwp.Services
{
    public interface IFilesAddress
    {
        Task<Models.ImageModel> GetThumbnail(string thumbnailLink);
        Task<List<String>> GetThumbnailsAddresses();
        void DownloadFiles(List<Uri> addresses);
    }
}