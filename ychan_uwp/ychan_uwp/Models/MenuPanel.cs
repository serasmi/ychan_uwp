﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ychan_uwp.Models
{
    public class MenuPanel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private bool isPaneOpen { get; set; }

        public bool IsPaneOpen
        {
            get { return this.isPaneOpen; }
            set
            {
                this.isPaneOpen = value;
                this.OnPropertyChanged();
            }
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public MenuPanel(bool DefaultValue)
        {
            IsPaneOpen = DefaultValue;
        }
    }
}
