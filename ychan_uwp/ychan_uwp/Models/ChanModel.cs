﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ychan_uwp.Models
{
    public class ChanModel
    {
        public ulong no { get; set; }
        public string now { get; set; }
        public string name { get; set; }
        public string com { get; set; }
        public string filename { get; set; }
        public string ext { get; set; }
        public ushort w { get; set; }
        public ushort h { get; set; }
        public byte tn_w { get; set; }
        public byte tn_h { get; set; }
        public ulong tim { get; set; }
        public ulong time { get; set; }
        public string md5 { get; set; }
        public uint fsize { get; set; }
        public ulong resto { get; set; }
        public byte bumplimit { get; set; }
        public byte imagelimit { get; set; }
        public string semantic_url { get; set; }
        public uint replies { get; set; }
        public uint images { get; set; }
        public ulong unique_ips { get; set; }
    }
}
