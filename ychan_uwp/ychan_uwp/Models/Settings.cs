﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ychan_uwp.Models
{
    public class Settings : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private string downloadsPath { get; set; }
        public string Downloads
        {
            get { return this.downloadsPath; }
            set
            {
                this.downloadsPath = value;
                this.OnPropertyChanged();
            }
        }

        private string downloadedPath { get; set; }
        public string Downloaded
        {
            get { return this.downloadedPath; }
            set
            {
                this.downloadedPath = value;
                this.OnPropertyChanged();
            }
        }

        private string maximumSpeed { get; set; }
        public string MaximumSpeed
        {
            get { return this.maximumSpeed; }
            set
            {
                this.maximumSpeed = value;
                this.OnPropertyChanged();
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public Settings()
        {
            object downloads;
            if(!AppSettings.Get("downloadsPath", out downloads))
            {
                AppSettings.AddOrUpdate("downloadsPath", UserDataPaths.GetDefault().Downloads);
                AppSettings.Get("downloadsPath", out downloads);
            }

            object downloaded;
            if(!AppSettings.Get("downloadedPath", out downloaded))
            {
                AppSettings.AddOrUpdate("downloadedPath", UserDataPaths.GetDefault().Downloads);
                AppSettings.Get("downloadedPath", out downloaded);
            }

            object maxSpeed;
            if(!AppSettings.Get("maximumSpeed",out maxSpeed))
            {
                AppSettings.AddOrUpdate("maximumSpeed", "Bez ograniczeń");
                AppSettings.Get("maximumSpeed", out maxSpeed);
            }

            Downloads = downloads.ToString();
            Downloaded = downloaded.ToString();
            MaximumSpeed = maxSpeed.ToString();
        }

        public void SaveSettings()
        {
            AppSettings.AddOrUpdate("downloadsPath", Downloads);
            AppSettings.AddOrUpdate("downloadedPath", Downloaded);
            AppSettings.AddOrUpdate("maximumSpeed", MaximumSpeed);
        }

        public List<string> GetInternetSpeeds
        {
            get { return new List<string>(this.InternetSpeeds.Keys); }
        }

        private Dictionary<string, double> InternetSpeeds = new Dictionary<string, double>()
        {
            {"48 KB/s",  48000},
            {"64 KB/s",  64000},
            {"96 KB/s",  96000},
            {"128 KB/s",  128000},
            {"192 KB/s",  192000},
            {"256 KB/s",  256000},
            {"384 KB/s",  384000},
            {"512 KB/s",  512000},
            {"768 KB/s",  768000},
            {"1 MB/s",  1000000},
            {"1.5 MB/s",  1500000},
            {"2 MB/s",  2000000},
            {"3 MB/s",  3000000},
            {"5 MB/s",  5000000},
            {"7 MB/s",  7000000},
            {"10 MB/s",  10000000},
            {"25 MB/s",  25000000},
            {"Bez ograniczeń",  0}
        };
    }
}
